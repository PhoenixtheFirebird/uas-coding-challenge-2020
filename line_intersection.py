# param: two 2D coordinates of a line, the center coordinates and radius of the circle, the numerical relations 
# between start and end coordinates does not matter  
# return the number of intersections if they intercept 
# method of calculation of the distance between the center of  circle and the line is referenced from 
# https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line

# import for test cases
import unittest

# two dimensional case
def intersection_calculator_2d(start_line_x, start_line_y, end_line_x, end_line_y, center_x, center_y
, radius):
    # derive the equation of the line with slope and y-intercept, and with reference to general line
    # expression: ax + by + c = 0, we see that y = mx + b => mx - y + b so that 
    # a = m, b = -1, c = b

    # first catch exceptions for slope
    if end_line_x - start_line_x == 0:
    # here the line will be x = q, where q is the constant and q = end_line_x = start_line_x
        # no intersection
        if not (center_x - radius <= end_line_x and center_x + radius >= end_line_x):
            print("no intersection!")
        # 1 intersection
        elif center_x - radius == end_line_x or center_x + radius == end_line_x:
            print("1 intersection!")
            return (end_line_x, center_y)
        # 2 intersections
        else:
            print("2 intersections!")
            return (end_line_x, center_y - (radius ** 2 - (end_line_x - center_x)**2)**0.5),(end_line_x, center_y + (radius ** 2 - (end_line_x - center_x)**2)**0.5)

    
    else:
        b = -1
        a = (end_line_y - start_line_y)/(end_line_x - start_line_x)
        c = start_line_y - a * start_line_x

        # the formula is |a* center_x + b * center_y + c|/ sqrt(a**2 + b**2)
        # since inputs are integers, we can neglect the effect of rounding  
        distance = abs(a*center_x + b * center_y + c)/(a**2 + b**2)**0.5

        # if distance greater than radius, no intersection
        if distance > radius:
            print("No intersection!")
            return

        # if distance equal to radius, 1 intersection, give the coordinates, 
        # the coordinate formulas can be found on the wiki page
        if distance == radius:
            print("1 intersection")
            return ((b*(b * center_x - a * center_y)- a * c)/(a**2 + b**2), 
            (a * (-b*center_x + a * center_y)-b*c)/(a**2 + b**2))

        # if distance smaller than radius, 2 intersections, give coordinates
        if distance < radius:
            print("2 intersections")

            # coordinate of the point closest to center 
            (a1, a2) = ((b*(b * center_x - a * center_y)- a * c)/(a**2 + b**2), 
                (a * (-b*center_x + a * center_y)-b*c)/(a**2 + b**2))

            # calculate the intersection points coordinates
            # pythhagorean for the distance to intersection, slope to calculate delta x
            delta_x = ((radius**2 - distance**2)/(a**2 + 1))**0.5
            delta_y = a * delta_x
            return (a1 + delta_x, a2 + delta_y),(a1 - delta_x, a2 - delta_y)


# test suit 

class intersection(unittest.TestCase):
    # when slope of line is not infinite:
    def test_regular_slope(self):
        # no intersection 
        self.assertEqual(intersection_calculator_2d(-1, 0, 1, 2, 10, -10, 4), None)
        # 1 intersection (also flat line)
        self.assertEqual(intersection_calculator_2d(4, 0, 10, 0, 5, 5, 5), (5.0,0.0))
        self.assertEqual(intersection_calculator_2d(0, 0, 4, 2, 4, -2, 4)[0], (4.0,2.0))
        # need tuple unpacking to test when there are two intersections
        self.assertAlmostEqual(intersection_calculator_2d(0, 0, 4, 2, 4, -2, 4)[1][0], 0.8)
        self.assertAlmostEqual(intersection_calculator_2d(0, 0, 4, 2, 4, -2, 4)[1][1], 0.4)
    # infinite slope 
    def test_infinite_slope(self):
        # no intersection
        self.assertEqual(intersection_calculator_2d(5,-10,5,10,10,2,4), None)
        # 1 intersection
        self.assertEqual(intersection_calculator_2d(0,0,0,100,1,0,1), (0,0))
        # 2 intersections, unpack tuples
        self.assertEqual(intersection_calculator_2d(12,0,12,1,13,1,6)[0][0], 12)
        self.assertAlmostEqual(intersection_calculator_2d(12,0,12,1,13,1,6)[0][1], -4.916)
        self.assertEqual(intersection_calculator_2d(12,0,12,1,13,1,6)[1][0], 12)
        self.assertAlmostEqual(intersection_calculator_2d(12,0,12,1,13,1,6)[1][1], 6.916)
        



    
        
if __name__ == '__main__':
    unittest.main()




# in regards to path planning, I searched over google and came across this repository: https://github.com/AtsushiSakai/PythonRobotics
# they basically have everything we need for path planning, and in the face of this I feel there is not need
# for writing path planning algorithms from scratch:) 


   


    


