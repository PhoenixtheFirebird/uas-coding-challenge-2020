# UAS-Coding-Challenge-2020: B-LNE-0 line intersection program 
note: there is no implementation of path planning in my program, but that was because after extensive thinking trying to come up with an efficient algorithm, I came across this github repository https://github.com/AtsushiSakai/PythonRobotics
online and it has every path planning algorithms we would ever want to implement in Python 3, so I feel there is no need for us to write algorithms from scratch:)

## test suit
here are the equations and graphs used in my test suit, covering all six possibilities
### when the slope of the line is not infinite:
- no intersection:
![Semantic description of image](Pictures/first.jpg "first")

- 1 intersection:
![Semantic description of image](Pictures/second.jpg "second")

- 2 intersections:
![Semantic description of image](Pictures/third.jpg "third")

### when the slope of the line is infinite:
- no intersection:
![Semantic description of image](Pictures/fourth.jpg "fourth")

- 1 intersection:
![Semantic description of image](Pictures/fifth.jpg "fifth")

- 2 intersections
![Semantic description of image](Pictures/sixth.jpg "sixth")